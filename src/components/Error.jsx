const Error = ({children}) => {
    return (
        <div className='bg-red-600 text-white text-center p-2 font-bold  text-sm mb-3 rounded'>
            {children}
        </div>
    )
}

export default Error
