import { useEffect } from "react";
import Pacientes from "./Pacientes"


function ListadoPacientes({ pacientes, setPaciente, eliminarPaciente }) {   

    return (
        <div className="md:w-1/2 lg:w-3/5 md:h-screen overflow-y-scroll">
            {pacientes && pacientes.length ? (
                <>
                <h2 className="font-black text-center text-3xl">Listado pacientes</h2>
                <p className="text-lg mt-5 mb-5 text-center">
                    Administra tus {''}
                    <span className="text-indigo-600 font-bold">Pacientes y citas</span>
                </p>
                </>

            ) : 
                <>
                <h2 className="font-black text-center text-3xl">No hay pacientes</h2>
                <p className="text-lg mt-5 mb-5 text-center">
                    Empieza agregando pacientes
                    <span className="text-indigo-600 font-bold"> y aparecerán en este espacio!</span>
                </p>
                </>
            }


            {pacientes.map(paciente => {
                return (
                    <Pacientes
                        key={paciente.id}
                        paciente={paciente}
                        setPaciente = {setPaciente}
                        eliminarPaciente = {eliminarPaciente}
                    />
                )

            })}

        </div>
    )


    
}



export default ListadoPacientes