import { useState, useEffect } from 'react';
import Error from './Error';

function Formulario({ pacientes, setPacientes, paciente, setPaciente }) {

  //Hook nombre mascota
  const [nombre, setNombre] = useState('');
  const [propietario, setPropietario] = useState('');
  const [email, setEmail] = useState('');
  const [fecha, setFecha] = useState('');
  const [sintomas, setSintomas] = useState('');

  const [error, setError] = useState(false);

  useEffect(() =>{
    if(Object.keys(paciente).length > 0){ //Validamos si vienes objeto de paciente
      setNombre(paciente.nombre)
      setPropietario(paciente.propietario)
      setEmail(paciente.email)
      setFecha(paciente.fecha)
      setSintomas(paciente.sintomas)
    }
  }, [paciente])




  const generarId = () =>{
    const random = Math.random().toString(36).substring(2); 
    const fecha = Date.now().toString(36);

    return random + fecha;
  } 

  const handleSubmit = (e) => {
    e.preventDefault();

    //Validacion formulario
    if ([nombre, propietario, email, fecha, sintomas].includes('')) {
      console.log('Hay un campo vacio')
      setError(true)
      return;
    }
    setError(false)

    //Objeto de paciente
    const objPaciente = {
      nombre,
      propietario,
      email,
      fecha,
      sintomas
    } 

    if(paciente.id){
      //Editando registro
      objPaciente.id = paciente.id

      const pacientesActualizados = pacientes.map( pacienteState => pacienteState.id === paciente.id ? objPaciente : pacienteState)

      setPacientes(pacientesActualizados);
      setPaciente({});

    }else{
      //Nuevo registro
      objPaciente.id = generarId();
      setPacientes([...pacientes, objPaciente]);
    }
  

    //Reiniciar formulario
    setNombre('')
    setPropietario('')
    setEmail('')
    setFecha('')
    setSintomas('')
  }

  return (
    <div className="md:w-1/2 lg:w-2/5 mx-5">
      <h2 className="font-black text-3xl text-center">Seguimiento Pacientes</h2>
      <p className="text-lg mt-5 mb-5 text-center">
        Añade pacientes y {''}
        <span className="text-indigo-600 font-bold">Administralos</span>
      </p>
      <form className="bg-white shadow-md rounded-lg py-10 px-5 mb-10" onSubmit={handleSubmit}>

        <div className="mb-5">
          <label htmlFor="mascota" className="block text-gray-700 uppercase font-bold">Nombre mascota</label>
          <input id="mascota" type="text" placeholder="Nombre de la mascota" className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={nombre} onChange={(e) => setNombre(e.target.value)}></input>
        </div>

        <div className="mb-5">
          <label htmlFor="propietario" className="block text-gray-700 uppercase font-bold">Nombre propietario</label>
          <input id="propietario" type="text" placeholder="Nombre del propietario" className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={propietario} onChange={(e) => setPropietario(e.target.value)}></input>
        </div>

        <div className="mb-5">
          <label htmlFor="correo" className="block text-gray-700 uppercase font-bold">Correo</label>
          <input id="correo" type="email" placeholder="Correo electrónico" className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={email} onChange={(e) => setEmail(e.target.value)}></input>
        </div>

        <div className="mb-5">
          <label htmlFor="fecha" className="block text-gray-700 uppercase font-bold">Fecha ingreso</label>
          <input id="fecha" type="date" placeholder="Correo electrónico" className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={fecha} onChange={(e) => setFecha(e.target.value)}></input>
        </div>


        <div className="mb-5">
          <label htmlFor="sintomas" className="block text-gray-700 uppercase font-bold">Sintomas</label>
          <textarea id="sintomas" type="text" placeholder="Describe los sintomas" className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={sintomas} onChange={(e) => setSintomas(e.target.value)} />
        </div>

        {error && <Error><p>Todos los campos son obligatorios</p></Error>}

        <input type="submit" className="bg-indigo-600 w-full p-3 text-white uppercase rounded-md font-bold hover:bg-indigo-700 cursor-pointer transition-all" value={paciente.id ? 'Editar paciente' : 'Agregar paciente'}></input>
      
      </form>
    </div>
  )
}

export default Formulario
